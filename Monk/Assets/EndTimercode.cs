﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTimercode : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GameObject.Find("Timercode").SendMessage("finished");
    }
}
